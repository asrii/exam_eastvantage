CREATE DATABASE exam;
USE exam;
CREATE TABLE blog (
    id INT(6) AUTO_INCREMENT PRIMARY KEY,
    title TEXT NOT NULL,
    author VARCHAR(60) NOT NULL,
    content TEXT NOT NULL ,
    filename TEXT NOT NULL,
    type VARCHAR(50) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    INDEX `blog_id` (`id`)
);
