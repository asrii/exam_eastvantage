<?php
    if(isset($_GET['id'])){
        if (trim($_GET['id']) !== '') {
            $sql = 'SELECT id,title,content,author,filename,type,created_at FROM blog WHERE id="'.mysql_real_escape_string($_GET['id']).'"';
        }

        $connection = mysqli_connect("localhost", "root", "", "exam");
        $res = mysqli_query($connection, $sql);
        while ($row = mysqli_fetch_row($res)) {
            $blog = $row;
        }
        $title = $blog[1]?:"";
        $content = $blog[2]?:"";
        $author = $blog[3]?:"";
        $filename = $blog[4]?:"";
        $type = $blog[5]?:"";
        $created_at = $blog[6]?:"";
        mysqli_close($connection);
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Optimy Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery-3.5.0.js"></script>
    <script src="js/bootstrap.js"></script>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script type="text/javascript">
        tinymce.init({
          selector: 'textarea#basic-example',
          height: 500,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
          ],
          toolbar: 'undo redo | formatselect | ' +
          'bold italic backcolor | alignleft aligncenter ' +
          'alignright alignjustify | bullist numlist outdent indent | ' +
          'removeformat | help',
          content_css: '//www.tiny.cloud/css/codepen.min.css'
        });
    </script>
</head>

<body>
    <!-- Navigation -->
    <?php include 'components/navmenu.php';?>

    <!-- Page Content -->
    <div class="container">
        <div class="row text-center">
            <?php if (empty($blog)) { ?>
                <header class="jumbotron mt-4 mx-auto mb-12">
                    <h1 class="display-3">Oops no entry for this blog id!</h1>
                    <a class="btn btn-primary btn-lg mt-4" href="/create-news.php">Create Article</a>
                </header>
            <?php }else{ ?>
            <div class="col-lg-12 col-md-12 mb-4 mt-2">
                <img src="uploads/<?=$filename?>" width="100%" height="100%">
                <h4><?=$title?></h4>
                <span><?=$author.":".$created_at?></span>
                <p>
                    <?=$content?>
                </p>
            </div>
            <?php } ?>
        </div>
    </div>

    <!-- Footer -->
    <?php include 'components/footer.php';?>
</body>
</html>
