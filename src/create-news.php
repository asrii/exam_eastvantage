<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fileinfo = @getimagesize($_FILES['file']['tmp_name']);
    $width = $fileinfo[0];
    $height = $fileinfo[1];
    
    $allowed_image_extension = array(
        "png",
        "jpg",
        "jpeg"
    );
    
    // Get image file extension
    $file_extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

    // Validate file input to check if is not empty
    if (! file_exists($_FILES['file']['tmp_name'])) {
        $response = "Choose image file to upload.";
    }    // Validate file input to check if is with valid extension
    else if (! in_array($file_extension, $allowed_image_extension)) {
        $response = " Your uploaded file is invalid. Pls Upload Only PNG and JPEG type.";
    }    // Validate image file size
    else if (($_FILES["file"]["size"] > 2000000)) {
        $response = "Image size exceeds 2MB";
    }    // Validate image file dimension
    else if ($width > "500" || $height > "300") {
        $response = "Image dimension should be within 500X300";
    } else {
        $filename = '';

        $target = __DIR__.'/uploads/'. basename($_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target)) {
            $filename = $_FILES['file']['name'];
            $sql = '
                INSERT INTO blog SET
                    title = "'.$_POST['title'].'",
                    content = "'.$_POST['content'].'",
                    type = "'.$_POST['type'].'",
                    author = "'.$_POST['author'].'",
                    filename = "'.$filename.'",
                    created_at = NOW()
            ';
            $connection = mysqli_connect("localhost", "root", "", "exam");
            $res = mysqli_query($connection, $sql);
            if ($res) {
                $response = "Article Successfully Added!";
            }else{
                $response = "Article Failed to Add!";
                mysqli_error($connection);
            }
            mysqli_close($connection);
            header('Location: /index.php');
        }else{
            $response = "Image Failed to upload!";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Optimy Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery-3.5.0.js"></script>
    <script src="js/bootstrap.js"></script>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script type="text/javascript">
        tinymce.init({
          selector: 'textarea#basic-example',
          height: 500,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
          ],
          toolbar: 'undo redo | formatselect | ' +
          'bold italic backcolor | alignleft aligncenter ' +
          'alignright alignjustify | bullist numlist outdent indent | ' +
          'removeformat | help',
          content_css: '//www.tiny.cloud/css/codepen.min.css'
        });
    </script>
</head>

<body>
    <!-- Navigation -->
    <?php include 'components/navmenu.php';?>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mt-2 mx-auto">

                <form method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="newsTitle">Title *</label>
                        <input type="text" class="form-control" maxlength="60" id="newsTitle" placeholder="Awesome Title" name="title" required>
                    </div>
                    <div class="form-group">
                        <label for="newContent">Content *</label>
                        <textarea id="basic-example" rows="15" name="content"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="newContent">Content Type</label>
                        <select class="form-control" id="contentType" name="type">
                            <option>Government</option>
                            <option>Food</option>
                            <option>Sports</option>
                            <option>Places</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="newsAuthor">Author *</label>
                        <input type="text" class="form-control" maxlength="60" id="newsAuthor" placeholder="Author Name" name="author" required>
                    </div>
                    <div class="form-group">
                        <?php if(!empty($response)) { ?>
                        <div class="response_error">
                            <?php echo $response; ?>
                        </div>
                        <?php }?>
                        <div class="custom-file">
                            <input type="file" onchange="preview()" id="validatedCustomFile" name="file" required>
                            <label class="custom-file-label" for="validatedCustomFile">Choose file... *</label>
                            <br/>
                            <span class="label label-default" style="font-size:10px;">Image size should not exceed 2MB
                            </span>
                        </div>
                        <img id="view" src="" />
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-outline-danger" href="/index.php">Cancel</a>
                </form>
            </div>
        </div>
    </div>

    <?php include 'components/footer.php';?>
    
    <script type="text/javascript">
        var thumb = document.getElementById("view");
        function preview() {
          thumb.src=URL.createObjectURL(event.target.files[0]);
        }
    </script>
</body>
</html>
