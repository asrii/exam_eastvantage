<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-brand" href="/index.php">Articles</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/index.php?filter=government">Government</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/index.php?filter=sports">Sports</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/index.php?filter=food">Food</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/index.php?filter=places">Places</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link btn btn-success" href="/create-news.php">Create</a>
            </li>
        </ul>
    </div>
</nav>