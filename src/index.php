<?php
    $sql = "SELECT id,title,content,author,filename,type,created_at FROM blog ORDER BY created_at DESC;";
    if(isset($_GET['filter'])){
        if (trim($_GET['filter']) !== '') {
            $sql = 'SELECT id,title,content,author,filename,type,created_at FROM blog WHERE type="'.mysql_real_escape_string($_GET['filter']).'" ORDER BY created_at DESC;';
        }
    }
    $connection = mysqli_connect("localhost", "root", "", "exam");
    $res = mysqli_query($connection, $sql);
    $blogs = [];
    while ($row = mysqli_fetch_assoc($res)) {
        $blogs[] = $row;
    }
    mysqli_close($connection);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Optimy Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="js/jquery-3.5.0.js"></script>
    <script src="js/bootstrap.js"></script>

</head>

<body>
    <!-- Navigation -->
    <?php include 'components/navmenu.php';?>

    <!-- Page Content -->
    <div class="container">
        <div class="row text-center">
            <?php if (empty($blogs)) { ?>
                <header class="jumbotron mt-4 mx-auto mb-12">
                    <h1 class="display-3">Oops no entry for this type!</h1>
                    <a class="btn btn-primary btn-lg mt-4" href="/create-news.php">Create Article</a>
                </header>
            <?php }else{ 
                foreach ($blogs as $blog) { ?>
            <div class="col-lg-3 col-md-6 mb-4 mt-2">
                <div class="card h-100">
                    <img class="card-img-top" src="uploads/<?=$blog['filename']?>" alt="<?=$blog['filename']?>">
                    <div class="card-body">
                        <h4 class="card-title"><?=$blog['title']?></h4>
                        <span><?=$blog['author']?></span>:<span><?=$blog['created_at']?></span>
                        <p class="card-text"><?=substr($blog['content'],0,100);?></p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary">Find Out More!</a>
                    </div>
                </div>
            </div>
            <?php }
            } ?>
        </div>
    </div>

    <!-- Footer -->
    <?php include 'components/footer.php';?>
</body>
</html>
